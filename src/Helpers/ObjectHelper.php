<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 15.02.16
 * Time: 23:00
 */

namespace Funwork\Serializer\Helpers;

class ObjectHelper
{

    private $object;

    /**
     * @var \ReflectionObject
     */
    private $reflectionObject;

    /**
     * @var array
     */
    private $resultArray;

    /**
     * @var \ReflectionProperty[]
     */
    private $properties;

    /**
     * @var string[]
     */
    private $staticProperties;

    public static function instance(): ObjectHelper
    {
        return new static();
    }

    public function getSerializableData($data): array
    {
        $this->checkObject($data);
        $this->object = $data;
        $this->reflectionObject = new \ReflectionObject($this->object);

        $this->loadProperties();
        $this->convertObject();

        return $this->resultArray;
    }

    public function setSerializableData($object, $data)
    {
        $this->object = $object;
        $this->resultArray = $data;
        $this->reflectionObject = new \ReflectionObject($this->object);

        $this->fillObject();

        return $this->object;
    }

    /**
     * @param $data
     *
     * @throws \LogicException
     */
    private function checkObject($data)
    {
        if (!is_object($data)) {
            throw new \LogicException('Object helper can work only with objects!');
        }
    }

    private function loadProperties()
    {
        $this->loadStaticProperties();

        $this->loadDynamicProperties();
    }

    private function convertObject()
    {
        $object = $this->object;

        $properties = [];
        array_walk(
            $this->properties,
            function (\ReflectionProperty $el) use ($object, &$properties) {
                if (!$el->isPublic()) {
                    $el->setAccessible(true);
                }

                $properties[$el->getName()] = $el->getValue($object);
            }
        );

        $this->resultArray = $properties;
    }

    private function loadDynamicProperties()
    {
        $this->properties = $this->reflectionObject->getProperties();
    }

    private function loadStaticProperties()
    {
        $this->staticProperties = $this->reflectionObject->getStaticProperties();
    }

    private function fillObject()
    {
        $this->fillProperties();
    }

    private function fillProperties()
    {
        foreach ($this->resultArray as $name => $value) {
            if (!$this->reflectionObject->hasProperty($name)) {
                continue;
            }

            $property = $this->reflectionObject->getProperty($name);
            if (!$property->isPublic()) {
                $property->setAccessible(true);
            }

            $property->setValue($this->object, $value);
        }
    }

}
