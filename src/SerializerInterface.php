<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 15.02.16
 * Time: 22:58
 */

namespace Funwork\Serializer;

interface SerializerInterface
{

    public function serialize($data): string;

    public function deserialize(string $data, $object = null);

}