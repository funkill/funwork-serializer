<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 15.02.16
 * Time: 22:59
 */

namespace Funwork\Serializer\Serializers;

use Funwork\Serializer\Helpers\ObjectHelper;
use Funwork\Serializer\SerializerInterface;

class JsonSerializer implements SerializerInterface
{
    public function serialize($data): string
    {
        $isObject = is_object($data);
        if ($isObject) {
            $data = ObjectHelper::instance()->getSerializableData($data);
        }

        return json_encode($data, $isObject ? JSON_FORCE_OBJECT : 0);
    }

    public function deserialize(string $data, $object = null)
    {
        $convertedData = json_decode($data, true);
        if (null === $object) {
            return $convertedData;
        }

        return ObjectHelper::instance()->setSerializableData($object, $convertedData);
    }

}