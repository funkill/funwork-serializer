<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 16.02.16
 * Time: 23:42
 */

namespace Serializers;


use Funwork\Serializer\FillableTestObject;
use Funwork\Serializer\Helpers\ObjectHelper;
use Funwork\Serializer\SerializerInterface;
use Funwork\Serializer\Serializers\JsonSerializer;
use Funwork\Serializer\TestObject;

class JsonSerializerTest extends \PHPUnit_Framework_TestCase
{

    private static $testArray = [
        "a" => 1,
        "b" => 2,
        1 => "a",
        2 => "b"
    ];
    private static $testArrayResult;

    private static $testObject;
    private static $testObjectResult;
    private static $fillableObject;

    /**
     * @var SerializerInterface
     */
    private $object;

    public static function setUpBeforeClass()
    {
        static::$testArrayResult = json_encode(static::$testArray);

        static::$testObject = new TestObject();
        static::$testObjectResult = json_encode(ObjectHelper::instance()->getSerializableData(static::$testObject));
        static::$fillableObject = new FillableTestObject();
    }

    public function setUp()
    {
        $this->object = new JsonSerializer();
    }

    public function testSerializeArray()
    {
        $this->assertEquals(
            static::$testArrayResult,
            $this->object->serialize(static::$testArray)
        );
    }

    public function testDeserializeArray()
    {
        $this->assertEquals(
            static::$testArray,
            $this->object->deserialize(static::$testArrayResult)
        );
    }

    public function testSerializeObject()
    {
        $this->assertEquals(
            static::$testObjectResult,
            $this->object->serialize(static::$testObject)
        );
    }

    public function testDeserializeObject()
    {
        $object = $this->object->deserialize(static::$testObjectResult, static::$fillableObject);
        $this->assertEquals(
            static::$testObjectResult,
            json_encode(ObjectHelper::instance()->getSerializableData($object))
        );
    }
}