<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 17.02.16
 * Time: 23:56
 */

namespace Funwork\Serializer;

class FillableTestObject
{

    const CONSTANT = 'const';

    private $notAvailablePrivateProperty;
    private $availablePrivateProperty;

    protected $notAvailableProtectedProperty;
    protected $availableProtectedProperty;

    public $availablePublicProperty;

    private static $notAvailablePrivateStaticProperty;
    private static $availablePrivateStaticProperty;

    protected static $notAvailableProtectedStaticProperty;
    protected static $availableProtectedStaticProperty;

    public static $availablePublicStaticProperty;

}