<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 15.02.16
 * Time: 23:29
 */

namespace Funwork\Serializer\Helpers;

use Funwork\Serializer\FillableTestObject;
use Funwork\Serializer\TestObject;

class ObjectHelperTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var ObjectHelper
     */
    private $object;

    private static $convertingObject;
    private static $convertingResult;
    private static $fillableObject;

    public static function setUpBeforeClass()
    {
        static::$convertingObject = new TestObject();
        static::$convertingResult = [
            "notAvailablePrivateStaticProperty" => "N/A",
            "availablePrivateStaticProperty" => "Private Static",
            "notAvailableProtectedStaticProperty" => "N/A",
            "availableProtectedStaticProperty" => "Protected Static",
            "availablePublicStaticProperty" => 'Public Static',
            "notAvailablePrivateProperty" => "N/A",
            "availablePrivateProperty" => "Private",
            "notAvailableProtectedProperty" => "N/A",
            "availableProtectedProperty" => "Protected",
            "availablePublicProperty" => 'Public',
        ];

        static::$fillableObject = new FillableTestObject();
    }

    public function setUp()
    {
        $this->object = ObjectHelper::instance();
    }

    public function testCheckObjectFailed()
    {
        $this->expectException(\LogicException::class);

        $method = $this->getMethod('checkObject');

        $method([]);
    }

    public function testCheckObject()
    {
        $method = $this->getMethod('checkObject');

        $method(new \Reflection());
    }

    public function testGetSerializableData()
    {
        $this->assertEquals(
            static::$convertingResult,
            $this->object->getSerializableData(static::$convertingObject)
        );
    }

    public function testSetSerializableData()
    {
        $serializedObject = $this->object->setSerializableData(
            static::$fillableObject,
            static::$convertingResult
        );

        $this->assertEquals(
            static::$convertingResult,
            $this->object->getSerializableData($serializedObject)
        );
    }

    private function getMethod($method)
    {
        $r = new \ReflectionObject($this->object);
        $reflectionMethod = $r
            ->getMethod($method);

        if (!$reflectionMethod->isPublic()) {
            $reflectionMethod->setAccessible(true);
        }

        return $reflectionMethod->getClosure($this->object);
    }
}
