<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 17.02.16
 * Time: 23:55
 */

namespace Funwork\Serializer;

class TestObject
{

    const CONSTANT = 'const';

    private $notAvailablePrivateProperty = 'N/A';
    private $availablePrivateProperty = 'Private';

    protected $notAvailableProtectedProperty = 'N/A';
    protected $availableProtectedProperty = 'Protected';

    public $availablePublicProperty = 'Public';

    private static $notAvailablePrivateStaticProperty = 'N/A';
    private static $availablePrivateStaticProperty = 'Private Static';

    protected static $notAvailableProtectedStaticProperty = 'N/A';
    protected static $availableProtectedStaticProperty = 'Protected Static';

    public static $availablePublicStaticProperty = 'Public Static';

}
